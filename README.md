## Py Web Heatmap

_**Developed for Gitlab Hackathon, not intended to be used in production applications**_

Uses a mobilenet based model to generate heatmap for website screenshots that predicts where the user will look for the first 5 seconds of him seeing the page. Intended to be used as above the fold.

Used as is.. device specific screenshots are to be provided. i.e. if you want a mobile device heatmap provide a mobile device screenshot

```bash
usage: webheatmap [-h] screenshot pally apikey output badge

Generates a human attention report based on website screenshot.

positional arguments:
  screenshot  path to load screenshot from
  pally       Pa11y json output report path
  output      path to save html file into
  badge       path to save badge svg file into
  apikey      gemini api key

options:
  -h, --help  show this help message and exit
```


### Additional Configs below
- Base Screenshot (To generate/overlay) = primary default option
- CTA generation using pytesseract and gemini in order to highlight Call to Actions in the website and check overlap
- Jinja page name and template used to generate a report
- Folder name for generating html, css and image into

### Outputs generated
- An HTML file with reported screen containing CTAs, Accessibility elements and heatmaps overlaid
- A Badge SVG with total usability score