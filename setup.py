from setuptools import setup

setup(
    name = 'py-web-heatmap',
    version = '0.1.1',
    packages = ['webheatmap'],
    package_data={'': ['weights.pth','template.jinja']},
    include_package_data=True,
    install_requires=[
      'setuptools-git',
      'argparse',
      'numpy',
      'pillow',
      'pytesseract',
      'requests',
      'jinja2',
      'anybadge',
      'torch',
      'torchvision',
      'opencv-python-headless'
   ],
    entry_points = {
        'console_scripts': [
            'webheatmap = webheatmap.__main__:main'
        ]
    })