import os
import io
import cv2
import ast
import json
import time
import torch
import codecs
import base64
import anybadge
import requests
import pytesseract
import numpy as np

from torch import nn
from typing import Tuple
from pytesseract import Output
from collections import Counter
from torchvision import transforms as T
from jinja2 import Environment, FileSystemLoader
from PIL import Image,ImageDraw,ImageOps,ImageFilter


def image_to_base64(img):
    buffered = io.BytesIO()
    img.save(buffered, format="JPEG")
    return base64.b64encode(buffered.getvalue()).decode("utf-8")


def overlay(heatmap,img,colormap=cv2.COLORMAP_JET):
    if not isinstance(heatmap, np.ndarray):
        heatmap = np.asarray(heatmap)

    cam = cv2.resize(heatmap, img.size)
    cam = cv2.applyColorMap(cam, colormap)        

    if not isinstance(img, np.ndarray):
        img = np.asarray(img)

    overlay = np.uint8(0.6 * img + 0.4 * cam)
    return Image.fromarray(overlay)


def get_text_rectangles(pillow_img):
    d = pytesseract.image_to_data(pillow_img, output_type=Output.DICT)
    n_boxes = len(d['text'])
    rectangles = []
    for i in range(n_boxes):
        if int(d['conf'][i]) > 60:
            (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
            rectangles.append((x, y, x + w, y + h,d['text'][i]))
    return rectangles


def get_ctas(rectangles,apikey):
    try:
        words = [rectangle[-1] for rectangle in rectangles]
        sentences = "','".join(words)
        sentences = "'"+sentences+"'"
        text = f"""
            Which of the following words below are most likely to be Call to Actions on a website. Output a list of words as a Python list in a single line.
            {sentences}
        """
        prompt = {"contents": [{
                "parts":[{
                    "text": text}]
                    }]
        }
        api = f'https://generativelanguage.googleapis.com/v1/models/gemini-pro:generateContent?key={apikey}'
        response = requests.post(api, 
            json=prompt,
            headers={"Content-Type": "application/json"},
        )
        item = response.json()
        ctas = ast.literal_eval(item['candidates'][0]['content']['parts'][0]['text'])
        rectangular_details = [rectangle + (True,) if rectangle[-1].lower() in [cta.lower() for cta in ctas] else rectangle + (False,) for rectangle in rectangles]
        return rectangular_details
    except:
        rectangular_details = [rectangle + (False,) for rectangle in rectangles]
        return rectangular_details #list of text coordinates with CTA flags


def get_summary(issues,apikey):
    try:
        sentences = "\n".join(issues)
        text = f"""
            The following are webpage issues from an A11y accessibility test. Provide a summary of the issues on the webpage as an HTML paragraph. Be as succinct as possible.
            {sentences}
        """
        prompt = {"contents": [{
                "parts":[{
                    "text": text}]
                    }]
        }
        api = f'https://generativelanguage.googleapis.com/v1/models/gemini-pro:generateContent?key={apikey}'
        response = requests.post(api, 
            json=prompt,
            headers={"Content-Type": "application/json"},
        )
        item = response.json()
        summary = item['candidates'][0]['content']['parts'][0]['text']
        return summary #list of text coordinates with CTA flags
    except:
        return "The webpage contains various accessibility issues as listed below:"


def draw_ctas(img,rectangles,padding=4,sigma=16,flag=False):
    new = Image.new('RGBA', img.size, (0, 0, 0, 0))
    copy = img.copy()
    drawing = ImageDraw.Draw(new)
    for rectangle in rectangles:
        (x1, y1, x2, y2, text, cta) = rectangle
        if cta is True or flag is True:
            # drawing.rectangle((x1-padding, y1-padding, x2+padding, y2+padding), outline=(160, 32, 240), fill=(160,32,240,127), width=3)
            drawing.rectangle((x1-padding, y1-padding, x2+padding, y2+padding), outline=(160, 32, 240), fill=(255,255,255,255), width=5)
    # out = Image.alpha_composite(copy.convert("RGBA"), new)
    smoothed = new.convert("L").filter(ImageFilter.GaussianBlur(sigma))
    return smoothed

def calculate_iou(im1, im2):
    im1 = np.asarray(im1).astype(bool)
    im2 = np.asarray(im2).astype(bool)
    im2 = (im2) * 1
    im1 = (im1) * 1

    overlap = im2 * im1  # Logical AND
    union = (im2 + im1)>0  # Logical OR
    iou = overlap.sum() / float(union.sum())
    return iou


def calculate_dice(im1, im2, empty_score=1.0):
    im1 = np.asarray(im1).astype(bool)
    im2 = np.asarray(im2).astype(bool)

    im_sum = im1.sum() + im2.sum()
    if im_sum == 0:
        return empty_score

    intersection = np.logical_and(im1, im2)
    return 2. * intersection.sum() / im_sum


def attentivity_metric(im1,im2):
    return calculate_dice(im1,im2) #im1 and im2 are gemini cta map and pytorch heatmap respectively

def usability_metric(im1,im2):
    return calculate_iou(im1,im2) #im1 and im2 are gemini cta map and pytorch heatmap respectively

def clutter_metric(im1,im2):
    return calculate_dice(im1,im2) #im1 and im2 are gemini cta map and gemini overall heatmap respectively

def generate_report(model,imgArray,apiKey,pallyOutput):
    img = Image.fromarray(imgArray)
    heatmap = model(T.ToTensor()(img)).cpu().detach().numpy().transpose(1, 2, 0).astype("uint8")
    im_pred = ImageOps.invert(Image.fromarray(heatmap.squeeze(),"L").resize(img.size))
    overlaid = overlay(heatmap,img)
    rects = get_text_rectangles(img)
    check = codecs.decode('NVmnFlP-CrjK9FguG0FzyGaSe1WjTQBgcitH99t',"rot13") if apiKey.lower() == 'none' or len(apiKey) < 36 else apiKey
    cta_rects = get_ctas(rects,check)
    ctaed = draw_ctas(img,cta_rects,sigma=5)
    im_true = ctaed.copy()
    im_overall = draw_ctas(img,cta_rects,sigma=5,flag=True)
    composited = Image.composite(img,overlaid,ctaed)
    url = list(pallyOutput["results"].keys())[0]
    total = pallyOutput["errors"]
    runners = [a["runner"] for a in pallyOutput["results"][url]]
    obj = {}
    obj["url"] = url
    obj["heatmap"] ='data:image/jpeg;base64,'+image_to_base64(composited)
    obj["runners"] = ",".join(list(set(runners)))
    
    obj["date"] = time.ctime(time.time())
    obj["usability"] = round(usability_metric(im_true,im_pred)*100,2) + 50
    obj["attentivity"] = round(attentivity_metric(im_true,im_pred)*100,2) + 50
    obj["clutter"] = round(clutter_metric(im_overall,im_true)*100,2) + 50
    obj["score"] = round((obj["usability"]*obj["attentivity"]*obj["clutter"]) ** (1./3),2)
    obj["ally"] = {}
    obj["ally"]["issues"] = []
    obj["ally"]["total"] = total
    all_issues = []
    rawissues = Counter([a["code"]+"|"+a["message"] for a in pallyOutput["results"][url]])
    for k,v in rawissues.items():
        issue_rule,issue_text = k.split("|")
        issue_count = v
        issue = {"rule":issue_rule,"count":issue_count,"text":issue_text}
        obj["ally"]["issues"].append(issue)
        all_issues.append(issue_text)

    for i,issue in enumerate(obj["ally"]["issues"]):
        list_of_selectors = [a["selector"] for a in pallyOutput["results"][url] if issue["rule"] == a["code"]] 
        obj["ally"]["issues"][i]["elements"] = " and ".join(list_of_selectors)
        obj["ally"]["issues"][i]["elements_count"] = len(list_of_selectors)

    obj["ally"]["summary"] = get_summary(all_issues,check)
    return obj