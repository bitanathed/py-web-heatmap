from jinja2 import Environment, FileSystemLoader
from .model import Net
from .functions import generate_report
import argparse
import anybadge
import torch
import json
import cv2
import os

parser = argparse.ArgumentParser(description ='Generates a human attention report based on website screenshot.')
parser.add_argument('screenshot',
                    type = str,
                    help ='path to load screenshot from')

parser.add_argument('pally',
                    help ='Pa11y json output report path',
                    type=str)
 
parser.add_argument('output',
                    help ='path to save html file into',
                    default= "./output.html",
                    type = str)

parser.add_argument('badge',
                    help ='path to save html file into',
                    default= "./badge.svg",
                    type = str)
 
parser.add_argument('apikey',
                help ='gemini api key',
                nargs='?',
                default='None',
                type=str)


def main():
    print('in main')
    args = parser.parse_args()
    screenshot = args.screenshot
    output_path = args.output
    badge_path = args.badge
    api_key = args.apikey
    pally = args.pally

    model = Net()
    weights_path = os.path.join(os.path.dirname(__file__), '.', 'weights.pth')
    state_dict = torch.load(weights_path,map_location="cpu")
    model.load_state_dict(state_dict)

    screenshot = cv2.cvtColor(cv2.imread(screenshot),cv2.COLOR_BGR2RGB)

    with open(pally) as f:
        pallyJSON = json.load(f)

    outputs = generate_report(model,screenshot,api_key,pallyJSON)
    template_path = os.path.join(os.path.dirname(__file__), '.')
    file_loader = FileSystemLoader(template_path)
    env = Environment(loader=file_loader)
    
    template = env.get_template("template.jinja")
    html = template.render(obj=outputs)
    with open(output_path,"w")  as f:
        f.write(html)
        f.close()

    thresholds = {55: 'red',
              60: 'orange',
              65: 'yellow',
              70: 'green'}

    badge = anybadge.Badge('Attention Score', outputs["score"], thresholds=thresholds)
    badge.write_badge(badge_path)
    print(f"Written HTML report to {output_path} and badge to {badge_path}")
if __name__ == '__main__':
    main()